/**
 * name: sort.c
 * 
 * build: run build.sh
 * run instructions: cd into build directory
 *     before attempting usage (below).
 * usage: ./sort <number of ints>
 * 
 * Takes as a command line argument the size of the
 * array and generates a randomly filled array of
 * integers.  The array is broken into two logical
 * subarrays and each is sorted by a thread using
 * qsort.  When the subarrays have been sorted,
 * a condition variable is signaled and a third 
 * thread runs qsort over entire array.
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>
#include "sort.h"

#define RANGE 1024
#define COUNT_LIMIT 2

static int count = 0;

pthread_mutex_t sort_lock;
pthread_cond_t sort_threshold_cv;

typedef struct
{
    int start_index;
    int nmeb;
    long int *arr;
} sort_data_t;

void fill_random(long int *array, long int size)
{
    long int i;
    srand(time(NULL));
    for (i = 0; i < size; i++)
    {
        array[i] = random() % RANGE;
    }
}

void print_array(long int *array, long int size)
{
    long int i;
    for (i = 0; i < size; i++)
    {
        printf("%ld ", array[i]);
    }
    printf("\n");
}

void *sort_half(void *arg)
{
    sort_data_t *d = (sort_data_t *) arg;
    // each thread sorts a disjoint subarray, no mutex needed for that
    qsort(d->arr + d->start_index, d->nmeb, sizeof(d->arr[0]), comp);
    // access to critical section
    pthread_mutex_lock(&sort_lock);
    count++; // must have sole access to count
    if (count == COUNT_LIMIT)
    {
        pthread_cond_signal(&sort_threshold_cv);
    }
    pthread_mutex_unlock(&sort_lock);
    pthread_exit(NULL);
}

void *sort_full(void *arg)
{
    sort_data_t *d = (sort_data_t *) arg;
    pthread_mutex_lock(&sort_lock);
    while (count < COUNT_LIMIT)
    {
        pthread_cond_wait(&sort_threshold_cv, &sort_lock);
        qsort(d->arr, d->nmeb, sizeof(d->arr[0]), comp);
    }
    pthread_mutex_unlock(&sort_lock);
    pthread_exit(NULL);
}

int comp(const void *n1, const void *n2)
{
    // returns > 0 when n1 is larger
    // returns   0 when n1 == n2
    // returns < 0 when n2 is larger
    long int a = *((long int *) n1);
    long int b = *((long int *) n2);

    return a - b;
}

int main(int argc, char *argv[])
{
    long int size = atoll(argv[1]);
    long int array_of_ints[size];
    int i;
    pthread_t tids[3];

    if (argc != 2)
    {
        fprintf(stderr, "usage: %s <number of ints>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    fill_random(array_of_ints, size);

    qsort(array_of_ints, size, sizeof(long int), comp);

    sort_data_t d1 = {0, size / 2, array_of_ints};
    sort_data_t d2 = {size / 2, ((size / 2) + (size % 2)), array_of_ints};
    sort_data_t d3 = {0, size, array_of_ints};

    pthread_mutex_init(&sort_lock, NULL);
    pthread_cond_init(&sort_threshold_cv, NULL);    
    
    pthread_create(&tids[0], NULL, sort_half, (void *) &d1);
    pthread_create(&tids[1], NULL, sort_half, (void *) &d2);
    pthread_create(&tids[2], NULL, sort_full, (void *) &d3);

    for (i = 0; i < 3; i++)
    {
        pthread_join(tids[i], NULL);
    }
    
    pthread_mutex_destroy(&sort_lock);
    pthread_cond_destroy(&sort_threshold_cv);
    pthread_exit(NULL);
}