#ifndef SORT_H
#define SORT_H
void print_array(long int *array, long int size);
void fill_random(long int *array, long int size);
void *sort_half(void *arg);
void *sort_full(void *arg);
int comp(const void *n1, const void *n2);
#endif /* SORT_H */